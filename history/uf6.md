## Ordre dd i hexdump

```
dnf -y install  hexdumpu


dd if=/dev/sda of=/tmp/mbr bs=512 count=1 status=progress
 |                                         --> te muestra el progreso de la copia
 |                                  
 |                                                                       --> nombre de grups que volem extreure 
 |                           
 |                         --> extraure bytes en grups de 512
 |              |
 |              --> of = output file => path al output file
 |
   --> if = input file => path al input file, si es un disk, /dev/sdX
        

hexdumpu /tmp/mbr


parted -s /dev/vda mklabel msdos
parted -s /dev/vda mkpart primary 1% 50%
parted -s /dev/vda mkpart primary 50% 60%
parted -s /dev/vda mkpart primary 60% 70%
parted -s /dev/vda mkpart extended 70% 100%
parted -s /dev/vda mkpart logical 70% 75%



## definir la política de aceptar todo lo que entra y sale por la red
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
## Flush y borrado de las reglas
iptables -F
iptables -X
# poner un password a root
passwd
# cambiar la distribución del teclado
setkmap es

```
# 1 des 2020

### Arranquem una màquina de SRCD a Isard 
### a la xarxa vlan 241

```
#per poder entrar a la màquina cal desactiva el firewall
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
## Flush y borrado de las reglas
iptables -F
iptables -X

#canviem password de root i la configuració del teclat
passwd
setkmap es

#mirem la ip amb 
ip a

#des de l'equip de l'aula ja podem entrar a la màquina virtual amb ssh
ssh root@10.200.241.XXX

# A la terminal ja podem escriure, copiar, enganxar comandes...

# per mirar com tenim la taula de particions:
lsblk

parted -s /dev/vda mklabel msdos
parted -s /dev/vda mkpart primary 4KB 5GB
parted -s /dev/vda mkpart primary 5GB 9GB
parted -s /dev/vda mkpart primary 9GB 13GB
parted -s /dev/vda mkpart extended 13GB 100%
parted -s /dev/vda mkpart logical 13GB 15GB
parted -s /dev/vda mkpart logical 17.5GB 21.5GB
parted -s /dev/vda mkpart logical 16.5GB 17.5GB


#per mirar les particions amb interface gràfica
# a la consola remote viewer o al vnc fem startx
# i arranquem el progrma gparted

```

#### Sistemes de fitxers

el sistema més antic es el fat16  (limitació en tamany màxim de la partició)
el sistema fat32 treu aquesta limiació de tamany de partició
sistema exFAT 

fat16
--> limitació de tamany màxim de partició

fat32
--> limitació de tamany màxim d'arxiu a 4G

exFAT
--> no hi ha limits
--> pensat per transferir fitxers amb pendrives
    sense control d'usuaris molt estrictes

ntfs
--> sistema de fitxers per controlar molt el nivell
    de permisos
    
ext2 / ext3 / ext4 => sistemes de fitxers de linux
                      més habituals tant per fitxers
                      com per sistema operatiu
                      
xfs, btrfs => sistema més nou de linux

swap => memòria d'intercanvi, quan et quedes sense
        ram es fa servir
































