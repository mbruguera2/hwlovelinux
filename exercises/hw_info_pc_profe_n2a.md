# Identificacion
* Equipo: ordeandor de profesor del **aula N2A**
* Día de extracción de información: **16 nov 2020**
* Autor: Alberto Larraz

# Extract hardware info from host with linux

## system

### system model, bios version and bios date, how old is the hardware?

para extraer la versión de bios usamos la orden dmidecode, la salida del comando es muy extensa y podemos filtrar por el tipo de información que buscamos.

```
[root@localhost ~]# dmidecode -t bios
# dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 2.7 present.

Handle 0x0000, DMI type 0, 24 bytes
BIOS Information
	Vendor: American Megatrends Inc.
	Version: F3
	Release Date: 03/18/2013
	Address: 0xF0000
	Runtime Size: 64 kB
	ROM Size: 4 MB
	Characteristics:
		PCI is supported
		BIOS is upgradeable
		BIOS shadowing is allowed
		Boot from CD is supported
		Selectable boot is supported
		BIOS ROM is socketed
		EDD is supported
		5.25"/1.2 MB floppy services are supported (int 13h)
		3.5"/720 kB floppy services are supported (int 13h)
		3.5"/2.88 MB floppy services are supported (int 13h)
		Print screen service is supported (int 5h)
		8042 keyboard services are supported (int 9h)
		Serial services are supported (int 14h)
		Printer services are supported (int 17h)
		ACPI is supported
		USB legacy is supported
		BIOS boot specification is supported
		Targeted content distribution is supported
		UEFI is supported
	BIOS Revision: 4.6

```
Para reducir la salida del comando puedo filtrar por los campos especifícos que buscamos:
```
[root@localhost ~]# dmidecode -s bios-version
F3
[root@localhost ~]# dmidecode -s bios-release-date
03/18/2013

```

* versión de bios: **F3**
* bios date: **03/18/2013**


## mainboard model, link to manual, link to product

Usamos la orden dmidecode pero con el tipo mainboard

```
asdfasdf
```

### memory banks (free or occupied)

### how many disks and types can be connected

### chipset, link to 
