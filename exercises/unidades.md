# Recordar
```
Podemos agrupar magnitudes en unidades grandes usamos Kilo / Mega / Giga / Tera 
 - En binario => 1TB = 1024 GB  / 1GB = 1024 MB / 1MB = 1024 KB / 1KB = 1024B
 - En decimal => 1TB = 1000 GB  / 1GB = 1000 MB / 1MB = 1000 KB / 1KB = 1000B

# Para diferenciar cuando hablamos en "binario" o en "decimal" se inventaron 
1TiB = 1024 GiB  / 1GiB = 1024 MiB / 1MiB = 1024 KiB / 1KiB = 1024B

PERO NO SE USA HABITUALMENTE, has de saber si hay que utilizar 1000 o 1024 en función del 
contexto: 
 - BINARIO: ficheros, tamaños de RAM...
 - DECIMAL: tamaños de discos duros y pendrives, velocidades en bits por segundo

1 byte = 1 Byte = 1B = 8 bits = 8 b

VELOCIDADES: bps (bits por segundo)
1 Gbps = 1000 Mbps / 1 Mbps = 1000 Kbps / 1 Kpbs = 1000 bps

VELOCIDADES: Bps (Bytes por segundo) 
1Bps = 8bps
```


# Ejercicios en clase de conversión de unidades y velocidades

```
Ideas importantes:
1 Byte = 8 bits
B = Bytesb = bit
Un fichero, un dato... se expres an B, KB, MB, GB, TB y las unidades se agrupan de 1024 en 1024 1TB = 1024 GB = 1024 * 1024 MB = 1024 * 1024 * 1024 KB = 1024 * 1024 * 1024 * 1024 B
EXCEPCIÓN!!!! si nos venden un disco que entonces la información comercial nos indica que tenemos agrupaciones de 1000 en 1000 1TB = 1000 GB = 1000 * 1000 MB = 1000 * 1000 * 1000 KB = 1000 * 1000 * 1000 * 1000 B

Cuando expresamos velocidades se puede hacer en bps (bits por segundo) o Bps  (Bytes por segundo)En este caso la forma de agrupar las unidades es x10001000 GBps = 1000 * 1000 MBps = 1000 * 1000 * 1000 KBps = 1000 * 1000 * 1000 * 1000 Bps = 1000 * 1000 * 1000 * 1000 * 8 bps
1000 Gbps = 1000 * 1000 Mbps = 1000 * 1000 * 1000 Kbps = 1000 * 1000 * 1000 * 1000 bps

1. Cuantos bits son 23.4 GBRespuestas posibles:a) 23400000000
b) 187200000000
c) 201004469453
d) 25125558682
e) 201004469452800

SOLUCIÓN: 23.4*1024*1024*1024*8 = 201004469452.8


2. cuanto tiempo tarda en transmitirse un fichero de 3GB en si la velocidad de transmisión es 100Mbps
respuestas posibles:
a: 4 minutosb: 4.3 minutosc: 32 segundosd: 4,1 minutoe: más de 5 minutos

Solución: 3*1024*1024*1024*8 / (100*1000*1000) / 60 = 4.294 minutos
3. cuando me venden un disco, nos toman el pelo y nos venden los GB como múltiplos de 1000, pero en realidad 1GB = 1024 MB = 1024 * 1024 KB = 1024 * 1024 * 1024 B. Tengo 2000 fotos de 2MB cada una. En la tienda de debajo de casa me venden un pendrive de 16GB, que parte de este pendrive quedará ocupado?
SOLUCIÓN:
Bytes_ocupados_en_fotos = 2000 * 2 * 1024 * 1024
Bytes_disponibles_en_pendrive = 16 * 1000 * 1000 * 1000
Porcentaje = Bytes_ocupados_en_fotos / Bytes_disponibles_en_pendrive = 0.262 = 26.2%
26,2 %16%24,4 %950%1.95 %317%2.09 %211%más del 30%16%
```


EJERCICIOS 9 dic

1. #me compro un pendrive de 64 GB, cuantas fotos de 8MB puedo guardar dentro discos y duros y pendrives... 

64*1000*1000*1000/(8*1024*1024) = 7629


2. #Me venden un disco de 1TB y tengo ficheros de 50MB de word que se pueden comprimir un 80% . ¿Cuantos ficheros comprimidos entran en el disco?

1*1000*1000*1000*1000/(50*0.20*1024*1024) = 95367


Mi fibra óptica va a 50Mbps. Si quiero descargar un fichero de 540MB cuanto tardará?

solución: (540*1024*1024*8) / (50 * 1000 * 1000) = 90.59 segundos


solución: 
